import Foundation

// Three types of responsibilities: personal coaching, group coaching, managing
// Two types of allocations: baseline, temporary
// Task: For a given coach and a date interval, calculate the percent of time spent working on each responsibility type

// Each coach has 1 baseline
// Temp allocation: could be 0, 1+, has a fixed start & end time
// If no temp allocation, default to baseline

/*
Baseline:
    Personal Coaching: 50%
    Managing: 50%

Temporary: 2020-03-15 - 2020-03-15
    Managing: 100%

The report for 2020-03-14 - 2020-03-15
    Personal coaching: 25%
    Managing: 75%
*/

class Coach: NSObject {
  enum Responsibility {
    case personalCoaching(Double)
    case groupCoaching(Double)
    case managing(Double)
  }
  
  struct TempResponsibility {
    let responsibilities: [Responsibility]
    let startDate: Int
    let endDate: Int
  }
  
  var baselineResponsibilities: [Responsibility] = []
  var tempResponsibilities: [TempResponsibility] = []
  
  func generateReport(_ startDate: Int, _ endDate: Int) {
    var sum: [Responsibility] = []
    
    for i in startDate...endDate {
       if let tempResponsibility = isTempResponsibilityActive(on: i) {
         sum.append(contentsOf: tempResponsibility.responsibilities)
       } else {
         sum.append(contentsOf: baselineResponsibilities)
       }
    }
    
    var personalCoachingTotal: Double = 0.0
    var groupCoachingTotal: Double = 0.0
    var managingTotal: Double = 0.0
    
    for responsibility in sum {
      switch responsibility {
        case .personalCoaching(let percent):
          personalCoachingTotal += percent
        case .groupCoaching(let percent):
          groupCoachingTotal += percent
        case .managing(let percent):
          managingTotal += percent
      }
    }
    
    let fullTotal = personalCoachingTotal + groupCoachingTotal + managingTotal
    
    print("Personal coaching: \(personalCoachingTotal / fullTotal * 100)%")
    print("Group coaching: \(groupCoachingTotal / fullTotal * 100)%")
    print("Managing: \(managingTotal / fullTotal * 100)%")
  }
  
  func isTempResponsibilityActive(on date: Int) -> TempResponsibility? {
    return tempResponsibilities.first(where: { $0.startDate <= date && date <= $0.endDate })
  }
  
}

let coach = Coach()
coach.baselineResponsibilities = [.personalCoaching(50.0), .managing(50.0)]
coach.tempResponsibilities = [Coach.TempResponsibility(responsibilities: [.managing(100.0)], startDate: 15, endDate: 15)]
coach.generateReport(14, 15)
